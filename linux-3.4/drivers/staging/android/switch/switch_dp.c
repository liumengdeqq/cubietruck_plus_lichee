/*
 *
 * Copyright(c) 2014-2016 Allwinnertech Co., Ltd.
 *      http://www.allwinnertech.com
 *
 * Author: liushaohua <liushaohua@allwinnertech.com>
 *
 * switch driver for toshiba 358777xgb detect
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */

#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/init.h>
#include <linux/interrupt.h>
#include <linux/platform_device.h>
#include <linux/slab.h>
#include <linux/workqueue.h>
#include <linux/gpio.h>
#include <linux/switch.h>
#include <linux/irq.h>
#include <mach/gpio.h>
#include <linux/pinctrl/consumer.h>
#include <linux/pinctrl/pinconf-sunxi.h>
#include <mach/sys_config.h>

extern s32 disp_switch(int screen_id, int output_type, int output_mode);

struct dp_switch_data {
	struct switch_dev sdev;
	struct device 		*dev;
	int state;
	struct work_struct  detect_dp;
};

static int dp_flag = 0;

static struct workqueue_struct *dp_detect_queue;
static script_item_u item_eint;
static int detect_irq = 0;
static struct dp_switch_data *dpdev;
int screen0_output_type = 0;

static ssize_t switch_dp_print_state(struct switch_dev *sdev, char *buf)
{
	struct dp_switch_data	*switch_data =
		container_of(sdev, struct dp_switch_data, sdev);

	return sprintf(buf, "%d\n", switch_data->state);
}


static irqreturn_t dp_detect_handler(int irq,void *dev_id)
{
	struct dp_switch_data * switch_data = (struct dp_switch_data *)dev_id;
	if(0 == queue_work(dp_detect_queue,&(switch_data->detect_dp))){
		printk("[dp detect ]add work struct ");
	}
	return IRQ_HANDLED;
}

static void dp_switch_work(struct work_struct *work)
{
	struct dp_switch_data	*switch_data =
		container_of(work, struct dp_switch_data, detect_dp);
	unsigned int status;
	int ret = 0;
	int screen_id = 1;

	if(screen0_output_type == 1){
		screen_id = 0;
	}else{
		screen_id = 1;
	}
	status = gpio_get_value(item_eint.gpio.gpio);
	if (status == 1) {
		printk("[Dp] Plug in\n");
			//open display port
			//ret = disp_switch(screen_id, 1, 0);//android上，hwc检测到dp插入，会去打开dp，所以在android上不用这句话也行
			printk("[Dp] dp_flag =%d, switch ret =%d,line=%d\n",dp_flag,ret,__LINE__);
			switch_data->state = 1;
	}
	else if(status == 0) {
		printk("[Dp] Plug out\n");
		//close display port
		//ret = disp_switch(screen_id, 0, 0);
		printk("[Dp] dp_flag =%d, switch ret =%d,line=%d\n",dp_flag,ret,__LINE__);
		switch_data->state = 0;
	}
	switch_set_state(&switch_data->sdev, switch_data->state);//android上，hwc检测到dp拔出，会去关闭dp，所以在android上不用这句话也行
}

static int dp_switch_probe(struct platform_device *pdev)
{

	script_item_value_type_e  type;
	int ret = 0;

	printk("hello DP SWITCH DRIVER \n");
	dpdev = kzalloc(sizeof(struct dp_switch_data), GFP_KERNEL);
	if (!dpdev){
		printk("dp_switch_probe: not enough memory for this device\n");
		return -ENOMEM;
	}

	dev_set_drvdata(&pdev->dev, (void *)dpdev);
	dpdev->sdev.name = "dp";
	dpdev->state = 0;
	dpdev->sdev.print_state = switch_dp_print_state;
	dpdev->dev = &pdev->dev;

	INIT_WORK(&dpdev->detect_dp, dp_switch_work);
	dp_detect_queue = create_singlethread_workqueue("detect_dp");

  type = script_get_item("disp_init", "screen0_output_type", &item_eint);
  if (SCIRPT_ITEM_VALUE_TYPE_INT != type) {
		printk("[DP] script_get_item return screen0_output_type err\n");
	}else{
		screen0_output_type = item_eint.val;
	}
  
	type = script_get_item("dp_para", "dp_int_ctrl", &item_eint);
	if (SCIRPT_ITEM_VALUE_TYPE_PIO != type) {
		printk("[DP] script_get_item return type err\n");
	}
	detect_irq = gpio_to_irq(item_eint.gpio.gpio);
	if (IS_ERR_VALUE(detect_irq)) {
		printk("[DP] map gpio to virq failed, errno = %d\n",detect_irq);
		return -EINVAL;
	}

	ret = switch_dev_register(&dpdev->sdev);
	if (ret < 0)
		goto err_dev_register;


	ret = request_irq(detect_irq,dp_detect_handler, IRQF_TRIGGER_FALLING|IRQF_TRIGGER_RISING, "detect_dp",dpdev);
	if (ret < 0) {
		printk("dp detect irq requsest irq faield\n");
		return -EINVAL;
	}

	ret = gpio_request(item_eint.gpio.gpio,NULL);



	return 0;

err_dev_register:
	kfree(&dpdev->sdev);

	return ret;
}

static int __devexit dp_switch_remove(struct platform_device *pdev)
{

	struct dp_switch_data *dpdev = platform_get_drvdata(pdev);

	//should free gpio
	if(dp_detect_queue)
		destroy_workqueue(dp_detect_queue);
	free_irq(detect_irq,NULL);
	gpio_free(item_eint.gpio.gpio);
	switch_dev_unregister(&dpdev->sdev);
	kfree(dpdev);
	return 0;
}

static struct platform_driver dp_switch_driver = {
	.probe		= dp_switch_probe,
	.remove		= __devexit_p(dp_switch_remove),
	.driver		= {
		.name	= "switch-display-port",
		.owner	= THIS_MODULE,
	},
};

static struct platform_device dp_switch_device = {
    .name = "switch-display-port",
};

static int __init dp_switch_init(void)
{
	int ret;
	if((ret = platform_device_register(&dp_switch_device)) < 0)
		return ret;
	if((ret = platform_driver_register(&dp_switch_driver)) < 0)
		return ret;
	return 0;
}

static void __exit dp_switch_exit(void)
{
	platform_driver_unregister(&dp_switch_driver);
	platform_device_unregister(&dp_switch_device);
}

module_init(dp_switch_init);
module_exit(dp_switch_exit);

MODULE_AUTHOR("hechuanlong");
MODULE_DESCRIPTION("Display-port Switch driver");
MODULE_LICENSE("GPL");
